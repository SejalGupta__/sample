import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: '35+ Programs Completed',
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
        Number of Programs(Workshops + Skilling Programs
      </>
    ),
  },
  {
    title: '30000+ Applicants',
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        Number of Applications for programs(Skilling+Workshops)
      </>
    ),
  },
  {
    title: '1000+ Students Trained',
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        Number of students trained in IoT and AI
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
<div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
            <h1 className="hero__title">Excited to contribute?</h1>
            <p className="hero__subtitle">Get started by Reading our contributer guide</p>
            <div className={styles.buttons}>
              <a href="https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/wikis/home">
                <div
                  className={classnames(
              		'button button--outline button--secondary button--lg',
              		styles.indexCtas,
                  )}>
                  Read Contributer Guide
                </div>
              </a>
            </div>
          </div>
        </div> 
      </main>
    </Layout>
  );
}


export default Home;
